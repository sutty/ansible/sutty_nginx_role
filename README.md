Sutty Nginx Role
================

Role for running a web server.

Role Variables
--------------

`data`: Data directory

`nginx_user`: UID

`nginx_group`: GID

`nginx_access_log_database`:
[access_log](https://0xacab.org/sutty/access_log) database connection
string.

`nginx_volumes`: Array of extra volumes to mount

`nginx_sites_templates`: Array of site names mapping to
`templates/NAME.j2` files.

`nginx_container_already_running`: Boolean indicating the container task
shouldn't be run.

`nginx_container_version`: Container version.

Example Playbook
----------------

```yaml
---
- hosts: "sutty"
  strategy: "free"
  remote_user: "root"
  tasks:
  - name: "sutty_nginx"
    include_role:
      name: "sutty_nginx"
    vars:
      nginx_user: "100"
      nginx_group: "82"
      nginx_access_log_database: "sqlite3:///var/log/access_log.sqlite3"
      nginx_volumes: []
```

License
-------

MIT-Antifa
